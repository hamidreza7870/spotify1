import React from 'react';
import { Route , Switch} from 'react-router-dom';
import './App.css';
import SpotifyCallback from './components/Navbar/SpotifyCallback';
import Categories from './components/Categories';
import Homepage from './components/Navbar/Homepage';
import Dashboard from './components/dashboard/index';
import Navbar from './components/Navbar/index';
import AuthRoute from './components/AuthRoute';
import Login from './components/Login/Login';
import NotFound from './components/NotFound';
import Category from './components/Category';
import Playlist from './components/PlayList';
import Player from './components/Player';
function App() {
  return (
	<div className="App">
			<Navbar />
		<Switch>
			<Route path="/" exact={true} component={Homepage} />
			<AuthRoute path="/albums/:playlistTrackAlbumId" component={Player}/>
			<AuthRoute path="/categories/:categoryId" component={Category}/>
			<AuthRoute path="/playlists/:playlistId" component={Playlist} />
			<AuthRoute path="/categories" component={Categories} />
			<Route path="/callback" component={SpotifyCallback} />
			<Route path='/login' component={Login} />
			<AuthRoute path="/dashboard" component={Dashboard} />
			<Route path='/' component={NotFound} />
		 </Switch>
	</div>
  );
}

export default App;

