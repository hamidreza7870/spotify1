import React from 'react'
import "./index.css"
import { ReactComponent as HomeIcon } from '../../assets/home-icon.svg';
import { ReactComponent as LibraryIcon } from '../../assets/library-icon.svg';
import { ReactComponent as SearchIcon } from '../../assets/search-icon.svg';
import { ReactComponent as SpotifyLogo } from '../../assets/spotify-logo.svg';
import { Link, withRouter } from 'react-router-dom';

function Dashboard(props) {
    const currentPath = props.match.path;
    return (
        <div className="dashboardGrid">
            <div className='dashboardSidebar'>
                    <Link to='/' >
                        <SpotifyLogo className='spotify'></SpotifyLogo>
                    </Link>
                <div className='item-container'>
                    <Link className='wrapper' to={`${currentPath}`} >
                        <HomeIcon className='homeIcon icon'/>
                        <h1 className='title home'>Home</h1>
                    </Link>

                    <Link className='wrapper' to={`${currentPath}/Search`}>
                        <SearchIcon className='icon searchIcon' />
                        <h1 className='title search'>Search</h1>
                    </Link>

                    <Link className='wrapper' to={`${currentPath}/Library`}>
                        <LibraryIcon className='icon libraryIcon'/>
                        <h1 className='title library'>Your Library</h1>
                    </Link>
                </div>
            </div>
            <div className='dashboardMain'>
            </div>
            <div className='dashboardPlayer'></div>

        </div>
    )
}
export default withRouter (Dashboard);