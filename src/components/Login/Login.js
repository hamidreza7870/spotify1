import React ,{ useEffect } from "react"
import LoginForm from './LoginForm';
import LoginWithSpotify from './LoginWithSpotify';
import "./Login.css"

function Login(props) {
    useEffect(() => {
        if (
            props.location &&
            props.location.from&&
            props.location.state.from
        )
        localStorage.setItem('referredRedirectPath', JSON.stringify(props.location.state.from))
    }, [props.location])
    
    return (
        <div className='login'>
        <LoginForm />
        <LoginWithSpotify />
        </div>
    )
}
export default Login;