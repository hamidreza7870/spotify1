import React from 'react';
import'./LoginWithSpotify.css';


 function LoginWithSpotify (props) {
  let yourclientid = '451764dd4587472590d910723e9e2beb';
    return (
      <button className='btnLogin-ws'>
        <a style={{color:'white'}} className='Link-ws'
          href={`https://accounts.spotify.com/authorize?client_id=${yourclientid}&response_type=token&redirect_uri=http://localhost:3000/callback/`}>
          Login With Spotify
        </a>
      </button>
    );

}

export default LoginWithSpotify;
