import React, {Component , useState } from 'react';
import { withRouter } from 'react-router-dom';
import "./LoginForm.css"
import { Formik, Field, Form, ErrorMessage } from 'formik';

let fakeLogin = (username, password) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      if (username === 'hamidreza' && password === '12345') {
        resolve({ status: 200, message: 'Logged in succesfuly' });
      } else {
        reject({ status: 401, message: 'Username/Password is wrong' });
      }
    }, 2000);
  });

export class LoginForm extends Component {
  state = {
    data: {},
    errors: {}
  };

  handleChange = e => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState(prevState => ({
      ...prevState,
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  handleSubmit = (values, { setSubmitting }) => {
    fakeLogin(values.username, values.password)
      .then(result => {
        console.log(result.message);
      })
      .catch(err => {
        console.error(err.message);
      })
      .finally(() => {
        setSubmitting(false);
      });
  };

  render() {
    return (
      <Formik
        className='formLogin'
        onSubmit={this.handleSubmit}
        initialValues={{ username: 'hamidreza', password: '12345' }}>
        {({ isSubmitting }) => (
          <Form style={{marginTop : '7.5em'}}>
            <label htmlFor="username">Username: </label>
            <Field style={{color: 'white'}} type="text" name="username" />
            <ErrorMessage name="username" />
            <label htmlFor="password">Password: </label>
            <Field style={{color: 'white'}} type="password" name="password" />
            <ErrorMessage name="password" />
            <input disabled={isSubmitting} type="submit" />
          </Form>
        )}
      </Formik>
    );
  }
}

export default withRouter(LoginForm);
