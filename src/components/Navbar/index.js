import React from 'react';
import { Link } from 'react-router-dom';
import './index.css'
import authUtils from '../utils/authUtils';


const AuthLink = ({ AuthComponent, UnauthComponent }) => {
    if (authUtils.isUserLoggedIn()) return AuthComponent;
    else return UnauthComponent;
  };

function Navbar(props) {
    return (
        <nav className='navigation'>
        <ul className='nav-ul'>
                <li className='nav-li'>
                    <Link className='nav-link' to="/login">
                    <AuthLink
                        AuthComponent={ <Link className='nav-link' to="/dashboard">Dashboard</Link> }
                        UnauthComponent={ <Link className='nav-link' to="/login">Login</Link> }
                    />
                    </Link>
                </li>
                
                     <Link className='nav-link' to="/categories">Categories</Link>
                
        </ul>
    </nav>
    )
}

export default Navbar;