import React from "react";
import {Redirect} from 'react-router-dom'
import authUtils from '../utils/authUtils';

function SpatifyCallback(props) {
    const localStorageReferredRedirectPath = localStorage.getItem('referredRedirectPath');
    localStorage.removeItem('referredRedirectPath');
    const redirectPath = localStorageReferredRedirectPath ? JSON.parse(localStorageReferredRedirectPath) : '/';


    const prop = new URLSearchParams(
         props.location.hash.slice(1)
    )
    const accessToken = prop.get('access_token')
    authUtils.login(accessToken);
    
    return (
        <>
            <Redirect to={redirectPath}/>
        </>
    )
}

export default SpatifyCallback;