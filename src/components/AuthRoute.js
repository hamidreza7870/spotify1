import React from 'react'
import authUtils from './utils/authUtils';
import { Route , Redirect } from 'react-router-dom'

function AuthRoute({ component : Component, ...rest }) {
    return (
      <Route
        {...rest}
        render={ props => authUtils.isUserLoggedIn()
          ? (<Component {...props} />)
          : (<Redirect to={{ pathname: "/login", state: { from: props.location } }} />)}
      />
    );
}

export default AuthRoute;