import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import authUtils from './utils/authUtils';
import './Player.css'

function Player(props) {
    const [isLoading, setIsLoading] = useState(true)
    const [icons, setIcons] = useState([])
    const [label, setLabel] = useState([])
    const [name, setName] = useState([])
    const [player, setPlayer] = useState([])

    let accessToken = authUtils.getToken();
    useEffect(() => {
        fetch(`https://api.spotify.com/v1${props.match.url}`, { headers: { authorization: `Bearer ${accessToken}` } })
        .then(res => res.json())
        .then(resjson => {
            setIsLoading(false);
            setPlayer(resjson.tracks.items)
            setIcons(resjson.images[0].url)
            setName(resjson.name)
            setLabel(resjson.label)
            console.log(resjson);
            
            })
    }, [accessToken, props.match.url])

    function Ms(ms){
        const a = 1000*Math.round(ms/1000)
        let d = new Date(ms)
        return d.getUTCMinutes() + ':' + d.getUTCSeconds()
    }

    return (
        <>
            <div className='player-container'>
                <div className='player-icon'>
                    <img className='player-pic' alt='pic' src={icons} />
                   <div className='player-title'> <h2 className='player-name'>{name}</h2> </div>
                    <p className='player-label'>{label}</p>
                </div>
                <ul className='player'>
                    {player.map(playlist => (
                        <li className='player-list' key={playlist.id}>
                            <a className='player-link' href={playlist.preview_url}>
                                <h5>{`${playlist.name}`} </h5>
                                <span className='player-duration'>{Ms(playlist.duration_ms)}</span>
                            </a>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    )
}

export default Player;