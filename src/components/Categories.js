import React, { useState, useEffect } from 'react';
import authUtils from './utils/authUtils';
import {withRouter , Link } from 'react-router-dom'
import './Categories.css'

function Categories(props) {
    const [isLoading, setIsLoading] = useState(true)
    const [categories, setCategories] = useState([])
    let accessToken = authUtils.getToken();
    useEffect(() => {
        fetch('https://api.spotify.com/v1/browse/categories', { headers: { authorization: `Bearer ${accessToken}` } })
        .then(res => res.json())
        .then(jsonres => {
            setIsLoading(false)
            setCategories(jsonres.categories.items)
        })
    }, [accessToken])

    return (
        <>
            {isLoading ? <p>loading...</p> : null}
            <ul className='categories'>
                {categories.map((category) => (
                    <li className='categories-list' key={category.id}>
                        <Link className='categories-link' to={`${props.match.path}/${category.id}`}>
                            <img src={category.icons[0].url} alt='icons' className='pic-categories'/>
                            {category.name}
                        </Link>
                </li>
                ))}
            </ul>

        </>
    )
}

            
export default withRouter(Categories) ;
