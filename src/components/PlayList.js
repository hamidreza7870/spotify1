import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import authUtils from './utils/authUtils';
import './PlayList.css'
function Playlist(props) {
    const [isLoading , setIsLoading] = useState(true)
    const [playlists, setPlaylists] = useState([])

    let accessToken = authUtils.getToken();
    useEffect(() => {
        fetch(`https://api.spotify.com/v1${props.match.url}/tracks`, { headers: { authorization: `Bearer ${accessToken}` } })
        .then(res => res.json())
        .then(resjson => {
            setIsLoading(false);
            setPlaylists(resjson.items)
            console.log(resjson);
            
        })
    }, [accessToken, props.match.url])
    
    return (
        <>
            <div className='category-container' style={{marginTop : '2em'}}>
                <ul className='category'>
                    {playlists.map(playlist => (
                        <li className='playlist-list' key={playlist.track.id}>
                            <Link className='category-link' to={`/albums/${playlist.track.album.id}`}>
                                <div className='category-description'>
                                    <img src={playlist.track.album.images[1].url} alt='icons' className='category-pic' />
                                    <div  className='category-name'>{playlist.track.name}({playlist.track.album.total_tracks})</div>
                                </div>
                            </Link>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    )
}

export default Playlist;