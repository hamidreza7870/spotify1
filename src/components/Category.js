import React, { useState, useEffect } from 'react';
import { Link , withRouter} from 'react-router-dom';
import authUtils from './utils/authUtils';
import './Category.css'

function Category(props) {
    const  [isLoading ,setIsLoading] = useState(true)
    const [icons, setIcons] = useState([])
    const [category, setCategory] = useState([])
    const [name , setName ] = useState([])

    let accessToken = authUtils.getToken();
    useEffect(() => {
        fetch(`https://api.spotify.com/v1/browse/categories/${props.match.params.categoryId}`, { headers: { authorization: `Bearer ${accessToken}` } })
        .then(res => res.json())
        .then(res => {
            setIsLoading(false);
            setIcons(res.icons[0].url)
            setName(res.name)
            // console.log(res);
            
        })
    }, [accessToken, props.match.params.categoryId, setIsLoading])
    
    useEffect(() => {
        fetch(`https://api.spotify.com/v1/browse/categories/${props.match.params.categoryId}/playlists`, { headers: { authorization: `Bearer ${accessToken}` } })
        .then(res => res.json())
        .then(resjson => {
            setIsLoading(false);
            setCategory(resjson.playlists.items);
            })
    }, [accessToken, props.match.params.categoryId, setIsLoading]) 

    return (
        <>
            <div className='category-container'>
                <h1 className='category-title'>{name}</h1>
                <ul className='category'>
                    {category.map(playlist => (
                        <li className='category-list' key={playlist.id}>
                            <Link className='category-link' to={`/playlists/${playlist.id}`}>
                                <div className='category-description'>
                                    <img src={playlist.images[0].url} alt='icons' className='category-pic' />
                                    <div className='category-name'> {playlist.name} ({playlist.tracks.total} ) </div>
                                    <div className='category-tagline'> {playlist.description} </div>
                                </div>
                            </Link>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    )
}

export default withRouter (Category);